package server

import (
	"github.com/gorilla/mux"
	"log"
	"multiScraperWS/scrapers"
	"multiScraperWS/scrapers/config"
	"net/http"
)

type Server struct {
	Router *mux.Router
}

type wsHandler struct {
	hub *scrapers.Hub
}

func New() *Server {
	r := &Server{
		Router: mux.NewRouter(),
	}
	r.Router.HandleFunc("/api/v1/helloworld", helloWorldHandler)
	return r
}

func (server *Server) Run() {
	hub := scrapers.NewHub()
	c := config.ReadConfig() // returns a list of config items to iterate over
	go hub.Run()
	for _, val := range c {
		hub.CreateTopic(val.Topic)
		p := scrapers.New(hub, val)
		go p.Poll()
	}
	server.Router.Handle("/ws", wsHandler{hub: hub}) // wsHandler has a handle on everything to do with the hub... which will store our pollers
	log.Fatal(http.ListenAndServe(":8082", server.Router))
}

func (wsh wsHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	scrapers.ServeWs(wsh.hub, w, r)
}
