package main

import (
	"multiScraperWS/server"
)

func main() {
	httpServer := server.New()
	httpServer.Run()
}
