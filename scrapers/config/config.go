package config

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
)

type Config struct {
	Url        string                 `json:"url"`
	Frequency  int                    `json:"frequency"`
	Method     string                 `json:"method"`
	Payload    map[string]interface{} `json:"payload"`
	Topic      string                 `json:"topic"`
	StatusCode int                    `json:"expected_status_code"`
	Timeout    int                    `json:"timeout"`
}

func ReadConfig() []Config {
	var configs []Config

	jsonFile, err := os.Open("scrapers/config/config.json")
	if err != nil {
		log.Println(err)
	}

	defer jsonFile.Close()
	byteValue, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		log.Println(err)
	}

	err = json.Unmarshal(byteValue, &configs)
	if err != nil {
		log.Println(err)
	}

	return configs
}
