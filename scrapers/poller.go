package scrapers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"io/ioutil"
	"log"
	"multiScraperWS/scrapers/config"
	"net/http"
	"time"
)

type Poller struct {
	Id     uuid.UUID
	Hub    *Hub
	Config config.Config
}

func New(hub *Hub, config config.Config) Poller {
	return Poller{Hub: hub, Id: uuid.New(), Config: config}
}

func (p Poller) Poll() {
	// do we want to poll all of the time, or only whenever a user connects? and so it saves external services
	for {
		time.Sleep(time.Duration(p.Config.Frequency) * time.Second)
		client := http.Client{
			Timeout: time.Duration(p.Config.Timeout) * time.Second,
		}

		requestBody, err := json.Marshal(p.Config.Payload)
		if err != nil {
			log.Println(err)
			continue
		}

		request, err := http.NewRequest(p.Config.Method, p.Config.Url, bytes.NewBuffer(requestBody))
		if err != nil {
			log.Println(err)
			continue
		}
		request.Header.Set("Content-Type", "application/json")

		resp, err := client.Do(request)
		if resp.StatusCode != p.Config.StatusCode {
			// the status code is incorrect
			log.Println("the statuscode returned, didn't match the status code in config")
			continue
		}

		if err != nil {
			log.Println(err)
			continue
		}

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Println(err)
			continue
		}

		var respJson interface{}

		err = json.Unmarshal(body, &respJson)
		if err != nil {
			log.Println(err)
			continue
		}

		fmt.Println(p.Config.Topic)
		msg := Message{Operation: "broadcast", Topic: p.Config.Topic, Content: respJson, Success: true}
		p.Hub.broadcast <- msg
		err = resp.Body.Close()
		if err != nil {
			log.Println(err)
			continue
		}
	}
}
