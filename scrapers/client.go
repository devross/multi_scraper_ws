package scrapers

import (
	"github.com/gorilla/websocket"
	"log"
	"net/http"
)

type Message struct {
	Operation string      `json:"operation"`
	Topic     string      `json:"topic"`
	Content   interface{} `json:"content"`
	Success   bool        `json:"success"`
}

type Client struct {
	hub   *Hub
	conn  *websocket.Conn
	topic string
	send  chan Message
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func (c *Client) readPump() {
	for {
		var msg Message
		err := c.conn.ReadJSON(&msg) // read our message off the client connection and convert to msg object
		if err != nil {
			c.hub.unregister <- c
			c.hub.unsubscribe <- c
			break
		}

		// interrogate the operation, depending on that, will depend on what we do.
		if msg.Operation == "subscribe" {
			// subscribe the client to the relevant topic
			c.topic = msg.Topic
			c.hub.subscribe <- c
			// send the message to the client
			c.send <- Message{Operation: "subscribe", Topic: c.topic, Success: true}
		} else if msg.Operation == "broadcast" {
			if c.topic == msg.Topic {
				c.hub.broadcast <- msg
			}
		}
	}
}

func (c *Client) writePump() {
	// write back to the Client
	for {
		select {
		case message, ok := <-c.send:
			if !ok {
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				c.hub.unregister <- c
				c.hub.unsubscribe <- c
				break
			}

			err := c.conn.WriteJSON(message)
			if err != nil {
				log.Println(err)
			}
		}
	}
}

func ServeWs(h *Hub, w http.ResponseWriter, r *http.Request) {
	upgrader.CheckOrigin = func(r *http.Request) bool { return true } // hacky way of getting around CORS

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}

	client := &Client{hub: h, conn: conn, send: make(chan Message), topic: ""}
	client.hub.register <- client
	// each client we serve, gets it's own write and read pump go routines
	go client.writePump()
	go client.readPump()
}
