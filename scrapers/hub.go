package scrapers

import (
	"fmt"
	"github.com/go-redis/redis"
	"multiScraperWS/goredis"
)

type topic struct {
	name        string
	subscribers *[]*Client
}

type Hub struct {
	clients     map[*Client]bool
	broadcast   chan Message
	register    chan *Client
	unregister  chan *Client
	subscribe   chan *Client
	unsubscribe chan *Client
	store       *redis.Client
	topics      *[]topic
}

func NewHub() *Hub {
	return &Hub{
		clients:    make(map[*Client]bool),
		broadcast:  make(chan Message), // broadcast to the clients
		register:   make(chan *Client), // register client to topic
		unregister: make(chan *Client), // unregister client from topic
		subscribe:  make(chan *Client), // subscribe client to topic - one client = one topic for now.
		store: redis.NewClient(&redis.Options{
			Addr:     "localhost:6379",
			Password: "", // no password set
			DB:       0,  // use default DB
		}),
		topics: &[]topic{
			{
				name:        "test_topic", // setup with initial test topic
				subscribers: &[]*Client{},
			},
		},
	}
}

func (h *Hub) Run() {
	for {
		select { // this will wait, until a channel receives an event
		case client := <-h.register: // register the client to the service
			h.clients[client] = true
		case client := <-h.unregister: // unregister the client from the service
			if _, ok := h.clients[client]; ok {
				delete(h.clients, client)
				close(client.send) // make sure we close all connections properly
			}
		case client := <-h.unsubscribe:
			for _, topic := range *h.topics {
				if topic.name == client.topic {
					for i, subscriber := range *topic.subscribers {
						if client == subscriber {
							(*topic.subscribers)[i] = (*topic.subscribers)[len(*topic.subscribers)-1]
							*topic.subscribers = (*topic.subscribers)[:len(*topic.subscribers)-1]
						}
					}
				}
			}
		case client := <-h.subscribe:
			// iterate over all the topics that we have
			for _, topic := range *h.topics {
				// if the client topic and the topic we've iterated onto are the same
				if topic.name == client.topic {
					var subscribed = false
					for _, subscriber := range *topic.subscribers {
						// does that topic already contain that client? If it does, then we need to tell them
						if subscriber == client {
							subscribed = true
							j := make(map[string]interface{})
							j["message"] = "You are already subscribed"
							client.send <- Message{Topic: client.topic, Content: j, Success: false}
						}
					}

					// if we're not subscribed, then append a subscriber to the slice of subscribers and send a Message onto the 'send' channel
					if !subscribed {
						*topic.subscribers = append(*topic.subscribers, client)
						// send below, uses the 'Operation' to then broadcast onto the appropriate channel
						client.send <- Message{Topic: client.topic, Operation: "subscribe", Success: true}
					}
				}
			}
		case message := <-h.broadcast: // wait for the h.broadcast channel to receive an event
			for client := range h.clients {
				// here, we'll use message.topic - to determine which topic this should be going to.
				for _, topic := range *h.topics {
					if client.topic == topic.name && message.Topic == topic.name {
						for _, subscriber := range *topic.subscribers {
							if client == subscriber {
								// the client, is subscribed to the topic of which the message was meant for.. so send it
								client.send <- message
								err := goredis.Set(h.store, client.topic, message)
								if err != nil {
									fmt.Println(err)
								}
							}
						}
					}
				}
			}
		}
	}
}

func (h *Hub) CreateTopic(topicName string) {
	*h.topics = append(*h.topics, topic{name: topicName, subscribers: &[]*Client{}})
}
