#!/bin/sh

tmux new-session -d -s stubs ;
tmux split-window -h ;
tmux select-pane -L
tmux send-keys -t stubs 'cd stubs && go run stub1.go' C-m
tmux select-pane -R
tmux send-keys -t stubs 'cd stubs && go run stub2.go' C-m
tmux attach-session -d -t stubs
tmux attach-session -d -t stubs