package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

type payload struct {
	Name     string `json:"name"`
	Password string `json:"password"`
	Email    string `json:"email"`
}

func httpJsonHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("Stub1 request received...")
	w.Header().Set("Content-Type", "application/json")
	p := payload{Name: "Ross Bown", Password: "password123", Email: "ross88399@outlook.com"}
	err := json.NewEncoder(w).Encode(p)
	if err != nil {
		log.Print("Failed to encode json")
	}
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/json", httpJsonHandler)
	log.Fatal(http.ListenAndServe(":8081", r))
}
