package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

type payload2 struct {
	Name     string `json:"name"`
	Password string `json:"password"`
	Email    string `json:"email"`
	Method   string `json:"method"`
}

func httpJsonHandler2(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {
		log.Println("Stub2 GET request received...")
		w.Header().Set("Content-Type", "application/json")
		p := payload2{Name: "Ross Bown", Password: "password1234", Email: "ross88399@outlook.com"}

		err := json.NewEncoder(w).Encode(p)
		if err != nil {
			log.Print("Failed to encode json")
		}
	} else if r.Method == http.MethodPost {
		log.Println("Stub2 POST request received...")
		decoder := json.NewDecoder(r.Body)
		var j interface{}

		err := decoder.Decode(&j)
		if err != nil {
			log.Println("Failed to parse JSON")
		}
		err = json.NewEncoder(w).Encode(j)
		if err != nil {
			log.Print("Failed to encode json")
		}
	}

}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/json", httpJsonHandler2)
	log.Fatal(http.ListenAndServe(":8083", r))
}
